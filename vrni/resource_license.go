package vrni

import (
	//"fmt"
	"log"

	//"strconv"

	"context"

	"github.com/hashicorp/terraform/helper/schema"
	vrni "github.com/vmware/go-vmware-vrni"
)

func resourceLicense() *schema.Resource {
	return &schema.Resource{
		Create: resourceLicenseCreate,
		Read:   resourceLicenseRead,
		Update: resourceLicenseUpdate,
		Delete: resourceLicenseDelete,
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},

		Schema: map[string]*schema.Schema{
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"license": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
		},
	}
}

func resourceLicenseCreate(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************Fake CREATE*****************************************************************")

	//	tierName := d.Get("name")
	//	appID := d.Get("appid").(string)
	client := m.(*vrni.APIClient)

	// Validate License
	log.Println("***********************validate************************")
	log.Println((d.Get("license")).(string))
	resp, err := client.ManagementApi.Validate(context.Background(), (d.Get("license")).(string))
	if err != nil {
		return nil
	}
	log.Println(resp)

	// Activate License
	resp, err = client.ManagementApi.Activate(context.Background(), (d.Get("license")).(string))
	if err != nil {
		return nil
	}
	log.Println(resp)

	var secret string
	secret, resp, err = client.ManagementApi.GetSecret(context.Background())
	log.Println(resp)
	log.Println("**********************SECRET*********************************")
	log.Println(secret)

	d.SetId(secret)

	//	nodes, resp, err := client.InfrastructureApi.ListNodes(context.Background())
	//	log.Println(nodes)
	//	log.Println(resp)
	//	log.Println(err)

	/*


		client := m.(*velocloud.APIClient)

		d.Set("activationkey", result.ActivationKey)
	*/
	return resourceLicenseRead(d, m)
}

func resourceLicenseRead(d *schema.ResourceData, m interface{}) error {
	log.Println("*********************************************** Application READ*****************************************************************")

	return nil
}

func resourceLicenseUpdate(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************Application UPDATE*****************************************************************")
	return nil
}

func resourceLicenseDelete(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************Application DELETE*****************************************************************")

	return resourceLicenseRead(d, m)
}
