package vrni

import (
	//"fmt"
	"log"

	"github.com/hashicorp/terraform/helper/schema"
	"github.com/hashicorp/terraform/terraform"
	api "github.com/vmware/go-vmware-vrni"
	//"context"
)

func Provider() terraform.ResourceProvider {
	return &schema.Provider{
		Schema: map[string]*schema.Schema{
			"host": &schema.Schema{
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("VRNI_HOST", nil),
			},
			"username": &schema.Schema{
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("VRNI_USERNAME", nil),
			},
			"password": &schema.Schema{
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("VRNI_PASSWORD", nil),
			},
		},
		ResourcesMap: map[string]*schema.Resource{
			"vrni_application": resourceApplication(),
			"vrni_tiers": resourceTiers(),
			"vrni_license": resourceLicense(),
			"vrni_threshold": resourceThreshold(),
		},

		ConfigureFunc: providerConfigure,
	}
}

func providerConfigure(d *schema.ResourceData) (interface{}, error) {
	host := d.Get("host").(string)
	username := d.Get("username").(string)
	password := d.Get("password").(string)

	cfg := api.Configuration{
		BasePath:  "https://" + host + "/api",
		Host:      host,
		Scheme:    "https",
		UserAgent: "terraform-provider-vrni/1.0",
	}

	client := api.NewAPIClient(&cfg)


	auth := api.UserCredential{
		Username: username,
		Password: password,
		Domain: &api.Domain{
			DomainType: "LOCAL",
		},
	}
	log.Println(auth)
	/*	
	// Connect and get token

	token, resp, err := client.AuthenticationApi.Create(context.Background(), auth)
	log.Print("******************************VRNI PROVIDER INIT***********************************")
	log.Println(token)
	log.Println(resp)
	log.Println(err)
	*/

	/*
		auth := velocloud.AuthObject{
			Username: username,
			Password: password,
		}

		// Connect and Get Cookie
		resp, err := client.LoginApi.LoginOperatorLogin(context.Background(), auth)
		if err != nil {
			return fmt.Println("Velocloud provider connectivity check failed")
		}
		log.Println(resp)
		client.AddCookie(string(resp.Cookies()[1].Raw))
	*/
	return client, nil
}
