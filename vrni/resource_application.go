package vrni

import (
	"fmt"
	"log"

	"context"

	"github.com/hashicorp/terraform/helper/schema"
	"github.com/hashicorp/terraform/helper/validation"
	vrni "github.com/vmware/go-vmware-vrni"
)

var MembershipTypeValues = []string{"IP_RANGE", "SEARCH"}
var PropertyValues = []string{"name", "IP Address"}

func resourceApplication() *schema.Resource {
	return &schema.Resource{
		Create: resourceApplicationCreate,
		Read:   resourceApplicationRead,
		Update: resourceApplicationUpdate,
		Delete: resourceApplicationDelete,
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},

		Schema: map[string]*schema.Schema{
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"tier": getTiersSchema(),
		},
	}
}


func getTiersSchema() *schema.Schema {
	return &schema.Schema{
		Type:        schema.TypeList,
		Description: "List of tiers in the section",
		Optional:    true,
		Elem: &schema.Resource{
			Schema: map[string]*schema.Schema{
				"name": &schema.Schema{
					Type:     schema.TypeString,
					Required: true,
				},
				"membershiptype": &schema.Schema{
					Type:     schema.TypeString,
					ValidateFunc: validation.StringInSlice(MembershipTypeValues, false),
					Required: true,
				},
				"property": &schema.Schema{
					Type:     schema.TypeString,
					ValidateFunc: validation.StringInSlice(PropertyValues, false),
					Required: true,
				},
				"value": &schema.Schema{
					Type:     schema.TypeString,
					Required: true,
				},
				"objecttype": &schema.Schema{
					Type:     schema.TypeInt,
					Default: 701,
					Optional: true,
				},

			},
		},
	}
}

func getTiersFromSchema(d *schema.ResourceData) []vrni.ApplicationTier {
	rules := d.Get("tier").([]interface{})

	log.Println("***********gettiers")
	log.Println(rules)
	var tierList []vrni.ApplicationTier
	for _, rule := range rules {
		data := rule.(map[string]interface{})
		log.Println(data)
		elem := vrni.ApplicationTier{
			Name: data["name"].(string),
			ModelKey: "",
			Criteria: []vrni.ApplicationCriteria{{
				IPRange: data["value"].(string),
				MembershipType: data["membershiptype"].(string),
				ObjectType: data["objecttype"].(int),
				Property: data["property"].(string),
				Value: data["value"].(string),
			}},
		}
		log.Println(fmt.Sprint(elem))

		tierList = append(tierList, elem)
	}
	return tierList
}

func resourceApplicationCreate(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************Application CREATE*****************************************************************")

	appName := d.Get("name")
	client := m.(*vrni.APIClient)
	tiers := getTiersFromSchema(d)

	log.Println("##################################application##")
	log.Println(tiers)

	body := vrni.ApplicationRequest{
		Name: appName.(string),
		Tiers: tiers,
	}

	res, ret, err := client.ApplicationsApi.AddApplication(context.Background(), body)
	log.Println(res)
	log.Println(ret)
	log.Println(err)
	d.SetId(res.Data.ModelKey)

	return nil
}

func resourceApplicationRead(d *schema.ResourceData, m interface{}) error {
	log.Println("*********************************************** Application READ*****************************************************************")
/*
	appName := d.Get("name")

	client := m.(*vrni.APIClient)

	results, _, _ := client.ApplicationsApi.ListApplications(context.Background(), nil)

	for _, v := range results.Results {
		res, _, _ := client.ApplicationsApi.GetApplication(context.Background(), v.EntityId)
		log.Println(res.Name)
		log.Println(res.EntityType)
		log.Println(res.EntityId)
		if res.Name == appName {
			log.Println(res.Name)
			log.Println(res.EntityType)
			log.Println(res.EntityId)
			d.SetId(res.EntityId)
		}
	}
*/
	return nil
}

func resourceApplicationUpdate(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************Application Update*****************************************************************")

	appName := d.Get("name")
	client := m.(*vrni.APIClient)
	tiers := getTiersFromSchema(d)

	log.Println("##################################application##")
	log.Println(tiers)

	body := vrni.ApplicationRequest{
		Name: appName.(string),
		ModelKey: d.Id(),
		Tiers: tiers,
	}

	res, ret, err := client.ApplicationsApi.AddApplication(context.Background(), body)
	log.Println(res)
	log.Println(ret)
	log.Println(err)
	d.SetId(res.Data.ModelKey)

	return nil
}

func resourceApplicationDelete(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************Application DELETE*****************************************************************")
	client := m.(*vrni.APIClient)

	client.ApplicationsApi.DeleteApplication(context.Background(), d.Id())
	log.Println("application deleted...........")

	return resourceApplicationRead(d, m)
}
