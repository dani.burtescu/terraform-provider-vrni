package vrni

import (
	//"fmt"
	"log"

	//"strconv"

	"context"

	"github.com/hashicorp/terraform/helper/schema"
	vrni "github.com/vmware/go-vmware-vrni"
)

func resourceThreshold() *schema.Resource {
	return &schema.Resource{
		Create: resourceThresholdCreate,
		Read:   resourceThresholdRead,
		Update: resourceThresholdUpdate,
		Delete: resourceThresholdDelete,
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},

		Schema: map[string]*schema.Schema{
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"application": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
		},
	}
}

func resourceThresholdCreate(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************Threshold CREATE*****************************************************************")

	name := d.Get("name")
	filter := "Application = " + d.Get("application").(string)
	//filter := string("Destination Cluster = 'Management' ")
	client := m.(*vrni.APIClient)
	
	body := vrni.ThresholdRequest {
		ConfigUI: vrni.ConfigUI {
			AggLevels: []string{"vmOrIp"},
			MetricEntity: "VM_IP",
			Metrics: []string{"net.usage.rate.average.kiloBitsPerSecond"},
			Name: name.(string),
			Properties: vrni.Properties {
				AggPeriod: "30",
				AggType: "none",
				Sensitivity: "Medium",
				ViolationType: "deviates",
			},
			ScopeFilterClauses: []string{filter},
		},
	}

	log.Println(body)


	res, ret, err := client.ThresholdApi.AddThreshold(context.Background(), body)
	log.Println(res)
	log.Println(ret)
	log.Println(err)
	d.SetId(res.Data.ModelKey)

	return resourceThresholdRead(d, m)
}

func resourceThresholdRead(d *schema.ResourceData, m interface{}) error {
	log.Println("*********************************************** Threshold READ*****************************************************************")
	/*
	name := d.Get("name")
	
		appName := d.Get("name")

		client := m.(*vrni.APIClient)

		results, _, _ := client.ApplicationsApi.ListApplications(context.Background(), nil)

		d.SetId("")
		for _, v := range results.Results {
			res, _, _ := client.ApplicationsApi.GetApplication(context.Background(), v.EntityId)
			log.Println(res.Name)
			log.Println(res.EntityType)
			log.Println(res.EntityId)
			if res.Name == appName {
				log.Println(res.Name)
				log.Println(res.EntityType)
				log.Println(res.EntityId)
				d.SetId(res.EntityId)
			}
		}
	*/

	return nil
}

func resourceThresholdUpdate(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************Threshold UPDATE*****************************************************************")
	return nil
}

func resourceThresholdDelete(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************Threshold DELETE*****************************************************************")
	client := m.(*vrni.APIClient)

	client.ApplicationsApi.DeleteThreshold(context.Background(), d.Id())
	log.Println("threshold deleted...........")

	return nil
}
